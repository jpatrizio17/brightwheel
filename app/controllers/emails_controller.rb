class EmailsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    @email = Email.new(email_params)
    if @email.save
      render status: 200
    else
      render json: @email.errors, status: :unprocessable_entity
    end
  end

  private

    def email_params
      params.permit([ :to, :to_name, :from, :from_name, :subject, :body ])
    end
end
