require 'uri'
require 'net/http'

class SendGridEmailSender
  attr_reader :email
  delegate :valid?, to: :email

  def initialize(email)
    @email = email
  end

  def send!
    if valid?
      uri = URI(ENV['SEND_GRID_URL'])
      https = Net::HTTP.new(uri.host, uri.port)
      https.use_ssl = true
      request = Net::HTTP::Post.new(uri.path)
      request.body = body_json.to_json
      request["Authorization"] = ENV['SEND_GRID_API_KEY']
      request["Content-Type"] = 'application/json'
      response = https.request(request)
      true
    else
      raise(ActiveRecord::RecordInvalid, email)
    end
  end

  def body_json
    {
      "personalizations" => [
        {
          "to" => [
            {
              "email" => email.to,
              "name" => email.to_name
            }
          ]
        }
      ],
      "from" => {
        "email" => email.from,
        "name" => email.from_name
      },
      "subject" => email.subject,
      "content" => [
        {
          "type" => "text/plain",
          "value"=> email.stripped_body
        }
      ]
    }
  end
end
