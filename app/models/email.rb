class Email
  include ActiveModel::Model
  attr_accessor :to, :to_name, :from, :from_name, :subject, :body
  validates :to_name, :from_name, :subject, :body, presence: true, allow_blank: false
  validates :to, :from, presence: true, format: /\w+@\w+\.{1}[a-zA-Z]{2,}/

  def save
    if valid?
      email_sender.send!
      Rails.logger.info "Delivered mail via #{client_display_name}"
      true
    else
      false
    end
  end

  def stripped_body
    body.strip
  end

  private

  def email_sender
    mail_gun? ? mail_gun_email_sender : send_grid_email_sender
  end

  def mail_gun?
    ENV['EMAIL_CLIENT'] == 'MAIL_GUN'
  end

  def client_display_name
    mail_gun? ? 'Mail Gun' : 'Send Grid'
  end

  def mail_gun_email_sender
    @mail_gun_email ||= MailGunEmailSender.new self
  end

  def send_grid_email_sender
    @send_grid_email ||= SendGridEmailSender.new self
  end
end
