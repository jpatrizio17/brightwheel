require 'uri'
require 'net/http'

class MailGunEmailSender
  attr_reader :email
  delegate :valid?, to: :email

  def initialize(email)
    @email = email
  end

  def send!
    if valid?
      uri = URI(ENV['MAIL_GUN_URL'])
      https = Net::HTTP.new(uri.host, uri.port)
      https.use_ssl = true
      request = Net::HTTP::Post.new(uri.path)
      request.basic_auth 'api', ENV['MAIL_GUN_API_KEY']
      request["Content-Type"] = 'application/json'
      request.set_form_data(form_data)
      response = https.request(request)
      true
    else
      raise(ActiveRecord::RecordInvalid, email)
    end
  end

  def form_data
    {
      to: "#{email.to_name} <#{email.to}>",
      from: "#{email.from_name} <#{email.from}>",
      subject: email.subject,
      text: email.stripped_body
    }
  end
end
