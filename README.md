# README #



### Frameworks ###
I chose Rails to set up a rest server. The only other framework I used is Webmock for testing.

I'm happy with the way the abstraction turned out. 
I wouldn't try to abstract any else until we have a few more use cases. 
If given more time I'd add model coverage. Since the business logic is fairly minimal the controller tests provide pretty good coverage.
I'd also add some more validation around edge cases and testing around validation. 

I didn't have a custom domain so mail gun will only send emails to verified address. Let me know if you want me to add one so you can test.

The one assumption I made to simplify this exercise is that request to third parties will always succeed. If this were a production app I would use a delayed job.

### How do I get set up? ###

To start server

```bundle install && rails s```

To run tests
```rake test```
