require 'test_helper'
require 'webmock/test_unit'

class EmailsControllerTest < ActionController::TestCase

  test "should send email via send grid by default" do
    send_grid_request = WebMock.stub_request(:post, "https://api.sendgrid.com/v3/mail/send").
      with(:body => "{\"personalizations\":[{\"to\":[{\"email\":\"to@example.org\",\"name\":\"to\"}]}],\"from\":{\"email\":\"from@example.org\",\"name\":\"from\"},\"subject\":\"test subject\",\"content\":[{\"type\":\"text/plain\",\"value\":\"test body\"}]}",
       :headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer 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', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).to_return(:status => 200, :body => "", :headers => {})

    post :create, to: 'to@example.org', to_name: 'to', from: 'from@example.org', from_name: 'from', subject: 'test subject', body: 'test body'

    WebMock.assert_requested send_grid_request
  end


  test "should send an email to mail gun when env is set to MAIL_GUN" do
    ENV['EMAIL_CLIENT'] = 'MAIL_GUN'
    mail_gun_request = WebMock.stub_request(:post, "https://api.mailgun.net/v3/sandbox24614971f04c4d4198832dd13d336e03.mailgun.org/messages").
      with(:body => {"from"=>"from <from@example.org>", "subject"=>"test subject", "text"=>"test body", "to"=>"to <to@example.org>"},
           :headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic YXBpOmtleS1hZGQzMTRjODcwNDIxYjA0NjQwYjgzNTRmZTFiYTNjNA==', 'Content-Type'=>'application/x-www-form-urlencoded', 'User-Agent'=>'Ruby'}).to_return(:status => 200, :body => "", :headers => {})

    post :create, to: 'to@example.org', to_name: 'to', from: 'from@example.org', from_name: 'from', subject: 'test subject', body: 'test body'

    WebMock.assert_requested mail_gun_request
    ENV['EMAIL_CLIENT'] = nil
  end

  test "renders a 422 when invalid" do
    post :create, to: 'to@example.org', to_name: 'to', from_name: 'from', subject: 'test subject', body: 'test body'

    assert_response 422
  end
end
